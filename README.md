# Notas de autómatas y lenguajes formales

Notas complementarias del curso de autómatas y lenguajes formales, de apoyo en la revisión de los temas faltantes.

Algunas de las notas las estaré actualizando por aquello de los errores, temas que considero debería incluirse, por si desean entrar y echar un ojo a posibles actualizaciones.

## Temario

1. Automatas finitos deterministas y no deterministas: *nfa_dfa.pdf*
2. Lema del bombeo: *bombeo.pdf*
3. Teorema de Myhill-Nerode: *myhill-nerode.pdf*
4. Minimización de automatas finitos deterministas-colapso de estados: *minimizacion.pdf*
5. Gramáticas libres de contexto, formas normales de Chomsky y Greibach: *gramaticas_libres_de_contexto.pdf*
6. Lema del bombeo para CFL y autómatas de pila: *lema_bombeo_pila.pdf*
7. Lenguajes de Dyck y teorema de Chomsky Schützenberger: *chomsky_schutzenberger.pdf*
8. Logaritmo Coke-Kasami-Younger *cky.pdf*
9. Máquinas de Turing (principios, diseño) *maquinas_turing.pdf*
10. Máquinas de Turing (reducción, teorema de Rice) *maquinas_turing_2.pdf*
11. Otros modelos *otros_modelos.pdf*

Cualquier cosa pueden escribir a [vladimir@ciencias.unam.mx](mailto:vladimir@ciencias.unam.mx), si desean una comunicación más directa me encuentran en xmpp vladimir@suchat.org (en el caso de haber llevado el curso ya saben otros métodos de contacto).

Para el curso se habilitó un canal de chat que sigue abierto para el uso de quien lo desee para dudas y discusiones en temas de teoría de la computación, son libres de usarlo respetando la temática del canal:
Canal en irc.libera.chat:##teoriacomputacion
O entrar por el cliente web: [https://kiwiirc.com/nextclient/irc.libera.chat/##teoriacomputacion](https://kiwiirc.com/nextclient/irc.libera.chat/##teoriacomputacion)

Sírvanse en tomar lo que les sea útil, cualquier comentario es bien recibido.
