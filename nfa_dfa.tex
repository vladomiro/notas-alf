\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{pgf}
\usepackage{tikz}
\usetikzlibrary{automata,positioning,arrows}
\renewcommand{\rmdefault}{ptm}
%\usepackage[all,cmtip]{xy}
%\usepackage{graphicx}
\author{Autómatas y Lenguajes Formales}
\title{Autómatas finitos deterministas y no deterministas}
\begin{document}
\maketitle
\section*{Autómata finito determinista}

Me iré un poco rápido en estas notas pues varios conceptos los hemos visto en clase (en caso de quien esto lea no haya asistido a la clase puede consultar la bibliografía al final del documento).

\newtheorem{defi}{Definición}
\begin{defi}
  Un autómata finito determinista es la quinteta 
  \begin{equation*}
    M = (Q,\Sigma,\delta,s,F)
  \end{equation*}
  
  \begin{itemize}
  \item $Q$ el conjunto finito de estados;
  \item $\Sigma$ conjunto finito, alfabeto de entrada;
  \item $\delta : Q\times \Sigma \rightarrow Q$ la función de transición;
  \item $s\in Q$ es el estado inicial;
  \item $F\subseteq Q$, los estados de aceptación o estados finales.
  \end{itemize}
\end{defi}
	 	
Veamos ejemplos para determinar que lenguaje acepta un autómata. Por ejemplo tenemos el siguiente autómata en diagrama:

\begin{figure}
  \begin{center}
    \begin{tikzpicture}
      \node[state,initial,initial text= ] (S)   {$S$}; 
      \node[state,accepting,above right=of S] (1) {$1$};
      \node[state,below right=of S] (2) {$2$};
      \node[state,below right=of 1] (3) {$3$};
      
      \path [-stealth,thick]
      (S) edge [bend left]  node [yshift=0.3cm]{$a$} (1)
      edge node[yshift=-0.3cm]{$b$} (2)
      (1) edge node [yshift=0.3cm]{$a$} (3)
      edge [bend left] node[yshift=-0.3cm]{$b$} (S)
      (2) edge [loop below] node{$a,b$} ()
      (3) edge node[yshift=-0.3cm]{$a,b$} (2);
      				
    \end{tikzpicture}
  \end{center}
\end{figure}

¿Cómo analizamos para saber qué cadenas acepta, y por lo tanto que lenguaje? Por lo regular es por pura observación, pero esa observación se reduce a probar varias cadenas y a ver cuales pegan.

Lo primero que notamos es que el alfabeto es $\{a,b\}$, el conjunto de los estados es $\{S,1,2,3\}$, el estado inicial es $S$ y sólo hay un final que es $1$. Propongo probar las cadenas: $\{a,b,ab,ba, aaa, bbb, aba, ababa\}$, a ver si podemos identificar algo. Para probar estas cadenas lo mejor es que ustedes en su pantalla o papel sigan con el dedo los estados a que lleva la lectura de cada carácter. Recuerden que lo único cercano a una memoria que tiene el autómata finito determinista son los distintos estados, así que la posición de su dedo guardará memoria de lo que ya se ha leído.

Para ejemplificar en papel ese seguimiento usaré la notación prestada de Lewis y Papadimitriou\cite{Lewis}, la entrada es leída como $(q,w)$ donde $q$ es el estado donde se encuentra y $w$  la subcadena que no se ha leído. Recuerden que la lectura se hace de izquierda a derecha, por convención. Las transiciones son marcadas por le símbolo $\vdash_A$ donde el subíndice $A$ hace referencia al autómata especificado.

De esta forma leemos la primera cadena propuesta:

\begin{align*}
  (S, a) &\vdash_A (1,\epsilon)
\end{align*}

Es decir, empezamos en el estado inicial $S$ con la cadena $a$, empieza la actividad del autómata al marcar $\vdash_A$: leemos $a$ y pasamos al estado $1$. Como ya no hay más cadena la salida es marcada como $(1, \epsilon)$, ya estamos en el estado $1$, que es final, y ya no hay nada más que leer, $\epsilon$ es la cadena vacía, es decir, nada.

Para el resto de cadenas:
La cadena $b$

\begin{align*}
  (S, b) &\vdash_A (2,\epsilon).
\end{align*}

Termina en el estado $2$ que no es final, es decir que la cadena no es aceptada.
La cadena $ab$

\begin{align*}
  (S, ab) &\vdash_A (1,b)\\
  (1, b) &\vdash_A (S,\epsilon)
\end{align*}

Termina en $S$ que no es de aceptación, la cadena no es aceptada.
Para $ba$

\begin{align*}
  (S, ba) &\vdash_A (2,a)\\
  (2, a) &\vdash_A (2,\epsilon)
\end{align*}

No es aceptada.
Para $aaa$

\begin{align*}
  (S, aaa) &\vdash_A (1,aa)\\
  (1, aa) &\vdash_A (3,a)\\
  (3,a) &\vdash_A (2,\epsilon)
\end{align*}

No es aceptada, lo mismo para $bbb$

\begin{align*}
  (S, bbb) &\vdash_A (2,bb)\\
  (2, bb) &\vdash_A (2,b)\\
  (2,b) &\vdash_A (2,\epsilon)
\end{align*}

Caso muy diferente con las últimas dos cadena, que sí son aceptadas: $aba$ y $ababa$:

\begin{align*}
  (S, aba) &\vdash_A (1,ba)\\
  (1, ba) &\vdash_A (S,a)\\
  (S,a) &\vdash_A (1,\epsilon)
\end{align*}

\begin{align*}
  (S, ababa) &\vdash_A (1,baba)\\
  (1, baba) &\vdash_A (S,aba)\\
  (S,aba) &\vdash_A (1,ba)\\
  (1, ba) &\vdash_A (S,a)\\
  (S,a) &\vdash_A (1,\epsilon)
\end{align*}

Los dos terminan en el estado $1$ que es de aceptación, ambas cadenas son aceptadas. Espero puedan ver de aquí una regularidad: se aceptan las cadenas que empiezan y terminan con $a$ pero no puede haber más de una $a$ ni una $b$ consecutiva. No es el autómata más pequeño que hace esa labor, pero así no lo dieron.

Como les había mencionado un sitio útil para probar los autómatas de manera computacional es \url{http://automatonsimulator.com/}, ya su experiencia en la programación les dirá que lo más sano es dudar de que haga todo correctamente, revisen por su cuenta y validen si lo que hace el programa es correcto.

Otra parte importante a ejercitar es diseñar el autómata a partir de la descripción del lenguaje, por ejemplo:

\begin{itemize}
  \item Partiendo del alfabeto $\{a,b\}$, construye el autómata que acepte las cadenas $w$ tales que $w$ tenga un número impar de $a$'s y termine en $b$.
\end{itemize}

Los estados y las transiciones nos serán úriles para llevar cuenta de las $a$'s y de la última $b$. Como las $b$'s intermedias no nos interesan tanto ya supondrán que eso lo podemos poner como un lazo (\emph{loop} como se le llama en inglés) sobre el estado. Esto se puede ver en la figura \ref{fig:1}.

\begin{figure}
  \begin{center}
    \begin{tikzpicture}
      \node[state] (q_n)   {$q_n$};
      
      \path [-stealth,thick]
      (q_n) edge [loop above]  node {$b$} ();
      					
    \end{tikzpicture}
  \end{center}
  \caption{Ejemplo de como aceptar en un estado cualquier cantidad de $b$'s o $b^*$. Sólo es un ejemplo, aún no está terminado el autómata.}
  \label{fig:1}
\end{figure}


¿Cómo hacemos que lleve cuenta de que la cantidad de $a$'s es impar? Puede parecer complicado pues números impares hay infinitos, pero hay un método más sencillo: al leer una $a$ pasamos a un estado, el que lleva la cuenta de la imparidad, si se lee otra $a$ se regresa al estado anterior, se lee la tercera a y se regresa al estado que lleva la cuenta, como se observa en la figura \ref{fig:2}

\begin{figure}
  \begin{center}
    \begin{tikzpicture}
      \node[state] (q_par)   {$q_{par}$};
      \node[state, right=of q_par] (q_impar)   {$q_{impar}$};
      
      \path [-stealth,thick]
      (q_par) edge [bend left]  node[yshift=0.3cm]{$a$} (q_impar)
      edge [loop above] node{$b$} ()
      (q_impar) edge [bend left]  node[yshift=-0.3cm]{$a$} (q_par);
      					
    \end{tikzpicture}
  \end{center}
  \caption{Ejemplo para identificar cantidades pares o impares de caracteres, sólo es un ejemplo no es un autómata terminado.}
  \label{fig:2}
\end{figure}

Con estos ejemplos podemos darnos una idea de como construir el autómata completo, sólo n0os haría falta comprobar la última $b$, me parece eso no requerirá mayor explicación, espero así sea. En la figura \ref{fig:3} se puede ver el autómata completo.

Quizá requiera una explicación extra sobre lo que pasa si ya estando en $q_2$ y se lee una $a$, como ya llevábamos la cuenta de $a$ impares, al leer una mas se tiene una cantidad par, regresamos a $q_0$ donde van las cantidades pares. Si la cadena termina ahí no es aceptada, de continuar se debe checar de nuevo que sea $a$ impar y termine con $b$.  

\begin{figure}
  \begin{center}
    \begin{tikzpicture}
      \node[state,initial,initial text= ] (q_0)   {$q_0$};
      \node[state, right=of q_0] (q_1)   {$q_1$};
      \node[state,accepting, right=of q_1] (q_2)   {$q_2$};
      
      \path [-stealth,thick]
      (q_0) edge [bend left]  node[yshift=0.3cm]{$a$} (q_1)
      edge [loop above] node{$b$} ()
      (q_1) edge [bend left]  node[yshift=-0.3cm]{$a$} (q_0)
      edge [bend left] node[yshift=0.3cm]{$b$} (q_2)
      (q_2) edge[bend left] node[yshift=-0.3cm]{$a$} (q_0)
      edge [loop above] node{$b$} ();
      					
    \end{tikzpicture}
  \end{center}
  \caption{Autómata completo que acepta las cadenas $w$ que contiene un número impar de $a$'s y terminan en $b$.}
  \label{fig:3}
\end{figure}

\section*{Autómatas finitos no deterministas}

\begin{defi}
  Un autómata finito no determinista es la quinteta 
  \begin{equation*}
    M = (Q,\Sigma,\Delta,S,F)
  \end{equation*}
  
  \begin{itemize}
  \item $Q$ el conjunto finito de estados;
  \item $\Sigma$ conjunto finito, alfabeto de entrada;
  \item $\Delta : Q\times \Sigma \rightarrow \mathcal{P}(Q)$ la función de transición;
  \item $S\subseteq Q$ conjunto se estados iniciales;
  \item $F\subseteq Q$, los estados de aceptación o estados finales.
  \end{itemize}
  
\end{defi}

Podemos ver un ejemplo sencillo. En la figura \ref{fig:4} tenemos un autómata no determinista de dos estados, con el alfabeto $\{a,b\}$.

\begin{figure}
  \begin{center}
    \begin{tikzpicture}
      \node[state,accepting,initial,initial text= ] (S)   {$S$};
      \node[state, right=of S] (q_1)   {$q_1$};
      
      \path [-stealth,thick]
      (S) edge [bend left]  node[yshift=0.3cm]{$a,b$} (q_1)
      edge [loop above] node{$a$} ()
      (q_1) edge [bend left]  node[yshift=-0.3cm]{$b$} (q_0);
      					
    \end{tikzpicture}
  \end{center}
  \caption{Ejemplo de autómata finito no determinista con alfabeto $\{a,b\}$ y dos estados.}
  \label{fig:4}
\end{figure}

De inspeccionar podemos ver que acepta las cadenas del tipo:
\begin{itemize}
  \item La cadena vacía $\epsilon$ (el estado inicial es de aceptación).
\item Las cadenas que son repeticiones de a $a,aa,aaa,aaaa,...$ es decir $a^*$ si incluimos aquí a la cadena vacía.
  \item Las subcadenas con un número par de caracteres siempre y cuando el segundo carácter se $b$, $ab,bb,abbb,bbbb,...$. Quizá ea más difícil de generalizar, yo lo pondría como $(ab)^+$ y $(bb)*$, líneas abajo veremos una forma más reducida de expresarlo.
\end{itemize}

¿Porqué es no determinista? Porque en es estado $S$ hay dos posibles salidas para $a$, o se queda en $S$ o pasa al estado $q_1$. Y en $q_1$ no hay transición para el carácter $a$. En este ejemplo hacen falta transiciones $\epsilon$.

Un ejemplo con transiciones $\epsilon$ se puede ver en la figura \ref{fig:5}. En este caso el autómata es no determinista porque hay estados que no tienen transición para ciertos caracteres (por ejemplo el $q_2$ que no tiene transición alguna de salida) y además hay una transición $\epsilon$ de $q_0$ a $q_1$. 

\begin{figure}
  \begin{center}
    \begin{tikzpicture}
      \node[state,accepting,initial,initial text= ] (q_0)   {$q_0$};
      \node[state, right=of q_0] (q_1)   {$q_1$};
      \node[state, right=of q_1] (q_2)   {$q_2$};
      
      \path [-stealth,thick]
      (q_0) edge [bend right]  node[yshift=-0.3cm]{$1$} (q_1)
      edge [bend right]  node[yshift=-0.3cm]{$\epsilon$} (q_2)
      (q_1) edge [bend right]  node[yshift=0.3cm]{$0$} (q_0)
      edge [bend left] node[yshift=0.3cm]{$0,1$}(q_2);
      					
    \end{tikzpicture}
  \end{center}
  \caption{Ejemplo de autómata finito no determinista con alfabeto $\{0,1\}$, tres estados y una transición $\epsilon$.}
  \label{fig:5}
\end{figure}

Inspeccionando el autómata podemos ver que las cadenas que acepta son:

\begin{itemize}
\item La cadena vacía $\epsilon$ ya que el estado inicial es de aceptación, pero hay un camino (la transición $\epsilon$ al estado $q_2$) en el que no se acepta.
  \item todas las repeticiones ordenadas de $10$, es decir $(10)^*$ incluyendo ya la cadena vacía, pero de igual forma esto sólo sucede en un camino posible, hay otro en el que no es aceptada
\end{itemize}

El resto de cadenas no son aceptadas. Las que consideramos que son aceptadas son aquellas en las que al menos en una posibilidad llegan a un estado de aceptación, pero hay posibilidades en las que no son aceptadas. Digamos que con que sea aceptada en uno basta para decir que son cadenas del lenguaje.

No muestro ejemplos de construcción pues son muy similares a las versiones deterministas salvo con algunas facilidades extras. En la mayoría de los casos es más fácil diseñar los autómatas como no deterministas.

\section*{Equivalencia entre autómatas finitos deterministas y no deterministas}

Hemos dicho que ambos tipos de autómatas finitos son equivalentes, al menos en el lenguaje que aceptan. La manera como lo aceptan, los estados extras y donde quedan las cadenas rechazadas puede variar mucho, pero a final de cuentas lo que nos interesa es el lenguaje que aceptan. Para pasar de un autómata finito no determinista ($N(Q,\Sigma,\Delta,q_0,F)$)\footnote{Hay autómatas finitos deterministas con más de un estado inicial, en ese caso con el apoyo de un estado nuevo y transiciones $\epsilon$ se puede con relativa facilidad convertir a un autómata finito no determinista con un sólo estado inicial.} a uno determinista ($A(Q',\Sigma,\delta,F')$) podemos seguir las siguientes instrucciones:

\begin{enumerate}
\item $Q'=\mathcal{P}(Q)=2^Q$, los estados del autómata finito determinista serán los elementos del conjunto potencia del conjunto de estados del autómata no determinista original, o el conjunto de todos los subconjuntos de $Q$.
\item Para los estados del autómata determinista, $R\in Q'$ y un carácter del alfabeto $a\in\Sigma$, la transición para el determinista estará dada por
  \begin{equation*}
    \delta(R,a)=\{q\in Q | q\in \Delta(r,a)\text{ para alguna } r\in R\}.
  \end{equation*}
  Como tenemos que los nuevos estados son elementos del conjunto potencia de los estados originales, existen estados $r\in Q$ tales que son elementos de un elemento $R\in Q'$. Una transición en el autómata no determinista $N$ puede llevar a más de un estado, por ello en el autómata determinista $A$ los llevamos a la unión de todos ellos:
  \begin{equation*}
    \delta(R,a)=\bigcup_{r\in R} \Delta(r,a)
  \end{equation*}

\item $q_0'=\{q_0\}$ por el momento dejamos el mismo estado inicial, pero no se acostumbren mucho a ello, va a cambiar un poco. 
\item $F'=\{R\in Q' | R\text{ contiene un estado de aceptación de } N\}$, es decir, todos los estados del autómata determinista $A$ que incluyen al menos uno de los estados finales del no determinista $N$ son estados finales.
\end{enumerate}

Y antes de aplicarlo hay que ver que se hace con las transiciones $\epsilon$:

\begin{itemize}
\item Definimos el conjunto $E(R)$ que contiene a todos los estados a los que se puede llegar desde $R$ a través de transiciones $\epsilon$, incluido $R$
  \begin{equation*}
    E(R)=\{ q| q \text{ se puede llegar desde }R\text{ viajando por } 0 \text{ o más transiciones }\epsilon \}.
  \end{equation*}
\item Modificamos las transiciones del autómata determinista $A$ para marcar las transiciones posibles por $\epsilon$, cambiando $\Delta(r,a)$ por $E(\Delta(r,a))$, es decir
  \begin{equation*}
    \delta(R,a)= \{ q\in Q| q\in E(\Delta(r,a))\text{ para algunas }r\in R \}
  \end{equation*}
  \item Nuestro nuevo estado inicial cambiara de $q_0'=\{q_0\}$ a $q_0'=E(q_0)$, todos los estados que pueden alcanzarse por transición $\epsilon$ dese el estado inicial original $q_0$.
\end{itemize}

Para ver esto en acción hagamos un ejemplo. Convertiremos el autómata de la figura \ref{fig:5} a su versión determinista. Vamos por partes:

\begin{enumerate}
\item Los estado originales son $Q=\{q_0,q_1,q_2\}$, por lo tanto los estados del nuevo autómata determinista serán
\begin{equation*}
  Q'=\{\phi,\{q_0\},\{q_1\},\{q_2\},\{q_0,q_1\},\{q_0,q_2\},\{q_1,q_2\},\{q_0,q_1,q_2\}\}.
\end{equation*}
\item Para ir viendo las transiciones hagamos una tabla:
  \begin{center}
    \begin{tabular}{| c || c | c |}
      \hline
      Estado & $0$ & $1$ \\ \hline
      $\phi$ & $\phi$  & $\phi$ \\
      $q_0^*$ (F) & $\phi$  & $q_1$ \\
      $q_1$ & $q_0\cup q_2$  & $q_2$ \\
      $q_2$ & $\phi$ & $\phi$\\
      $\{q_0,q_1\}$ (F) & $\phi \cup q_0\cup q_2=q_0\cup q_2$  & $q_1\cup q_2$ \\
      $\{q_0,q_2\}$ (F) & $\phi$  & $q_1\cup \phi=q_1$ \\
      $\{q_1,q_2\}$ & $q_0\cup q_2\cup \phi = q_0\cup q_2$ & $q_2\cup \phi = q_2$ \\
      $\{q_0,q_1,q_2\}$ (F) & $\phi\cup q_0\cup q_2\cup \phi = q_0\cup q_2$  & $q_1\cup q_2\cup \phi = q_1\cup q_2$ \\
      \hline
    \end{tabular}
  \end{center}
\item $q_0$ es el estado inicial marcado en la tabla como $q_0^*$. Esto puede cambiar.
\item Los estados finales son $F=\{\{q_0\},\{q_0,q_1\},\{q_0,q_2\},\{q_0,q_1,q_2\}\}$, todos los que incluyen a $q_0$. Están marcados como (F) en la tabla.
\end{enumerate}

Hasta ahí vamos bien, pero nos faltan las transiciones $\epsilon$, también las veremos por pasos como se indicó líneas arriba:

\begin{itemize}
\item Por suerte el único estado con transición $\epsilon$ es $q_0$ y sólo llega a $q_2$ (y al estado mismo), entonces

  \begin{equation}
    E(q_0) = \{q_0,q_2\}
  \end{equation}

\item Ahora cambiamos las transiciones, todas las que llegaba al estado $q_0$ ahora llegan al estado $E(q_0)$
  \begin{center}
    \begin{tabular}{| c || c | c |}
      \hline
      Estado & $0$ & $1$ \\ \hline
      $\phi$ & $\phi$  & $\phi$ \\
      $q_0$ (F) & $\phi$  & $q_1$ \\
      $q_1$ & $E(q_0)\cup q_2=\{q_0,q_2\}\cup q_2$  & $q_2$ \\
      $q_2$ & $\phi$ & $\phi$\\
      $\{q_0,q_1\}$ (F) & $E(q_0)\cup q_2= \{q_0,q_2\}\cup q_2$  & $q_1\cup q_2$ \\
      $\{q_0,q_2\}^*$ (F) & $\phi$  & $q_1$ \\
      $\{q_1,q_2\}$ & $E(q_0)\cup q_2= \{q_0,q_2\}\cup q_2$ & $q_2$ \\
      $\{q_0,q_1,q_2\}$ (F) & $E(q_0)\cup q_2= \{q_0,q_2\}\cup q_2$  & $q_1\cup q_2$ \\
      \hline
    \end{tabular}
  \end{center}
  \item Ahora debemos ver si cambia el estado inicial, y coincide en que justo el estado inicial tiene una transición $\epsilon$. Entonces el estado inicial cambia de $q_0$ a $E(q_0)=\{q_0,q_2\}$, información ya incluida en la tabla de líneas arriba.
\end{itemize}

Y listo, tenemos nuestro autómata finito determinista $A$ equivalente al finito no determinista $N$. Con eso bastaría, pero en caso de que sean más visuales de esa tabla de transiciones podemos armar el diagrama, pero eso lo agrego después, pueden hacerlo ustedes y me pasan como queda y lo agrego.

\section*{Expresiones regulares}

\begin{defi}
  Decimos que $R$ es una expresión regular si $R$ es
  \begin{enumerate}
  \item $a$ para alguna $a$ en el alfabeto $\Sigma$ ($\{a\}$),
  \item $\epsilon$ la cadena vacía ($\{\epsilon \}$),
  \item $\phi$ (el lenguaje vacío),
  \item $(R_1\cup R_2)$ o $(R_1 + R_2)$, donde $R_1$ y $R_2$ son expresiones regulares (unión de lenguajes),
  \item $(R_1\cdot R_2)=(R_1R_2)$ donde $R_1$ y $R_2$ son expresiones regulares (concatenación de lenguajes), o
  \item $(R_1^*)$ con $R_1$ una expresión regular (estrella de Kleene del lenguaje).
  \end{enumerate}
\end{defi}

De estas expresiones regulares podemos construir autómatas finitos no deterministas (por facilidad y hacerlo más directo, con un poco de análisis se puede hacer determinista), que pueden ser usados como células para armar expresiones más complicadas.

Por ejemplo para la expresión regular $a$ tenemos el autómata que se muestra en la figura \ref{fig:6}

\begin{figure}[h!]
  \begin{center}
    \begin{tikzpicture}
      \node[state,initial,initial text= ] (q_0)   {$q_0$};
      \node[state,accepting,right=of q_0] (q_1)   {$q_1$};
      
      \path [-stealth,thick]
      (q_0) edge node[yshift=0.3cm]{$a$} (q_1);
      					
    \end{tikzpicture}
  \end{center}
  \caption{Autómata que acepta el lenguaje determinado por la expresión regular $a$.}
  \label{fig:6}
\end{figure}

De aquí se figurarán que para la expresión regular $\epsilon$ basta con hacer el estado inicial final y no incluir transición alguna. Para el lenguaje regular definido por $\phi$ (el lenguaje vacío) se tiene un estado inicial sin transiciones y sin ningún estado final, el lenguaje aceptado es vacío, ni la cadena vacía acepta.

Quizá más interesante sea ver como construir el autómata para la unión, concatenación y estrella de Kleene de expresiones regulares. Para la unión de expresiones realizamos la unión de los autómatas que aceptan cada expresión, por ejemplo el término $(a+b)$ o $(a\cup b)$ se puede ver en la figura \ref{fig:7}

\begin{figure}[h!]
  \begin{center}
    \begin{tikzpicture}
      \node[state,initial,initial text= ] (q_ep)   {$q_{\epsilon}$};
      \node[state,above right=of q_ep] (q_0a)   {$q_{0_a}$};
      \node[state,accepting,right=of q_0a] (q_1a)   {$q_{1_a}$};
      \node[state,below right=of q_ep] (q_0b)   {$q_{0_b}$};
      \node[state,accepting,right=of q_0b] (q_1b)   {$q_{1_b}$};
      
      \path [-stealth,thick]
      (q_ep) edge node[yshift=0.3cm]{$\epsilon$} (q_0a)
      edge node[yshift=-0.3cm]{$\epsilon$} (q_0b)
      (q_0a) edge node[yshift=0.3cm]{$a$} (q_1a)
      (q_0b) edge node[yshift=-0.3cm]{$b$} (q_1b);
      					
    \end{tikzpicture}
  \end{center}
  \caption{Autómata que acepta el lenguaje determinado por la expresión regular $a+b$.}
  \label{fig:7}
\end{figure}

Se crea un nuevo estado inicial que sea para ambos autómatas y se pegan a través de transiciones $\epsilon$ a los antiguos estados iniciales de ambos autómatas. Los estados finales son los mismos.

Para el caso de la concatenación se usa igual transiciones $\epsilon$ como pegamento, solo que en este caso es de izquierda a derecha. Por ejemplo para la expresión $ab$ podemos ver en la figura \ref{fig:8} como se hace el ``pegado'' de los autómatas que aceptan los lenguajes de las expresiones $a$ y $b$.

\begin{figure}[h!]
  \begin{center}
    \begin{tikzpicture}
      \node[state,initial,initial text= ] (q_0a)   {$q_{0_a}$};
      \node[state,right=of q_0a] (q_1a)   {$q_{1_a}$};
      \node[state,right=of q_1a] (q_0b)   {$q_{0_b}$};
      \node[state,accepting,right=of q_0b] (q_1b)   {$q_{1_b}$};
      
      \path [-stealth,thick]
      (q_0a) edge node[yshift=0.3cm]{$a$} (q_1a)
      (q_1a) edge node[yshift=0.3cm]{$\epsilon$} (q_0b)
      (q_0b) edge node[yshift=0.3cm]{$b$} (q_1b);
      					
    \end{tikzpicture}
  \end{center}
  \caption{Autómata que acepta el lenguaje determinado por la expresión regular $ab$.}
  \label{fig:8}
\end{figure}

El siguiente caso por ver sería la estrella de Kleene de una expresión regular, si partimos del caso de la concatenación, por ejemplo $(ab)^*$, el autómata resultante sería el que se muestra en la figura \ref{fig:9}. Para hacer la estrella de Kleene basta con hacer una transición $\epsilon$. Para dejarlo de la forma más general, para no regresar al estado inicial se puede agregar un estado. Puede estar de más, o puede ser útil después, por si las moscas lo agregamos.

\begin{figure}[h!]
  \begin{center}
    \begin{tikzpicture}
      \node[state,initial,initial text= ] (q_ep)   {$q_{\epsilon}$};
      \node[state,accepting,right=of q_ep] (q_0a)   {$q_{0_a}$};
      \node[state,right=of q_0a] (q_1a)   {$q_{1_a}$};
      \node[state,right=of q_1a] (q_0b)   {$q_{0_b}$};
      \node[state,accepting,right=of q_0b] (q_1b)   {$q_{1_b}$};
      
      \path [-stealth,thick]
      (q_ep) edge node[yshift=0.3cm]{$\epsilon$} (q_0a)
      (q_0a) edge node[yshift=0.3cm]{$a$} (q_1a)
      (q_1a) edge node[yshift=0.3cm]{$\epsilon$} (q_0b)
      (q_0b) edge node[yshift=0.3cm]{$b$} (q_1b)
      (q_1b) edge[bend right] node[yshift=0.3cm]{$\epsilon$} (q_0a);
      					
    \end{tikzpicture}
  \end{center}
  \caption{Autómata que acepta el lenguaje determinado por la expresión regular $(ab)^*$.}
  \label{fig:9}
\end{figure}

Para el caso $(a\cup b)^*$ cambia un poco, pero se sigue la misma idea de las transiciones $\epsilon$ para adherir y hacer la estrella de Kleene, como se muestra en la figura \ref{fig:10}.

\begin{figure}[h!]
  \begin{center}
    \begin{tikzpicture}
      \node[state,initial,initial text= ] (q_eep)   {$q_{\epsilon \epsilon}$};
      \node[state,accepting,right=of q_eep] (q_ep)   {$q_{\epsilon}$};
      \node[state,above right=of q_ep] (q_0a)   {$q_{0_a}$};
      \node[state,accepting,right=of q_0a] (q_1a)   {$q_{1_a}$};
      \node[state,below right=of q_ep] (q_0b)   {$q_{0_b}$};
      \node[state,accepting,right=of q_0b] (q_1b)   {$q_{1_b}$};
      
      \path [-stealth,thick]
      (q_eep) edge node[yshift=0.3cm]{$\epsilon$} (q_ep)
      (q_ep) edge node[yshift=0.3cm]{$\epsilon$} (q_0a)
      edge node[yshift=-0.3cm]{$\epsilon$} (q_0b)
      (q_0a) edge node[yshift=0.3cm]{$a$} (q_1a)
      (q_1a) edge node[yshift=-0.3cm]{$\epsilon$} (q_ep)
      (q_0b) edge node[yshift=-0.3cm]{$b$} (q_1b)
      (q_1b) edge node[yshift=0.3cm]{$\epsilon$} (q_ep);
      					
    \end{tikzpicture}
  \end{center}
  \caption{Autómata que acepta el lenguaje determinado por la expresión regular $(a\cup b)^*$.}
  \label{fig:10}
\end{figure}

A partir de estas formas básicas se pueden construir los autómatas equivalentes a las expresiones regulares (equivalentes ya que aceptan el mismo lenguaje).

\begin{thebibliography}{10}
\bibitem{Kozen} Kozen, Dexter C. ``Automata and Computability'' Springer (1997)
\bibitem{Sipser} Sipser, Michael ``Introduction to the Theory of Computation'' 2a ed., Thomson Course Tecnology (2006)
\bibitem{Lewis} Lewis, Harry R. y Papadimitriou, Christos H. ``Elements of the Theory of Computation''2a ed., Prentice-Hall Inc. (1998).
  
  
\end{thebibliography}

\end{document}
